import { Instrument } from "./instrument";

export default class Flugelhorn extends Instrument {
    // Function to play a frequency for a given duration at a specific time
    playFrequency = (frequency: number, startTime: number, duration: number) => {
        if (this.audioContext == null) return;
        const oscillator = this.audioContext.createOscillator();
        const gainNode = this.audioContext.createGain();

        // Oscillator setup: Sawtooth wave for a brass-like sound
        oscillator.type = 'sawtooth';
        oscillator.frequency.setValueAtTime(frequency, startTime);

        // ADSR envelope using GainNode for a brass-like sound
        const attack = 0.2;  // slow attack
        const decay = 0.4;   // some decay
        const sustain = 0.7; // sustain level
        const release = 0.5; // slow release

        gainNode.gain.setValueAtTime(0, startTime);
        gainNode.gain.linearRampToValueAtTime(1, startTime + attack);
        gainNode.gain.exponentialRampToValueAtTime(sustain, startTime + attack + decay);
        gainNode.gain.setValueAtTime(sustain, startTime + duration);
        gainNode.gain.exponentialRampToValueAtTime(0.01, startTime + duration + release);

        // Connect and play
        oscillator.connect(gainNode);
        gainNode.connect(this.audioContext.destination);
        oscillator.start(startTime);
        oscillator.stop(startTime + duration + release);
    };
}
