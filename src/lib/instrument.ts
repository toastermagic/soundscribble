import Notes from "./notes";

export abstract class Instrument {
    chordInterval = 4;
    notes: any;
    height: number = 0;
    audioContext: AudioContext | null = null;
    sampleRate: number = 0;

    constructor(height: number) {
        this.notes = Notes.selectScaleByName("C# minor");
        this.height = height;
    }

    // Initialize AudioContext
    init() {
        this.audioContext = new window.AudioContext();
    };

    selectScale(name: string) {
        this.notes = Notes.selectScaleByName(name);
    }

    normalize(value: number, max_p: number, min_p: number, max: number, min: number) {
        return Math.floor(
            (max_p - min_p) / (max - min) * (value - max) + max_p
        );
    }

    // Function to calculate frequency based on equal temperament tuning
    calculateFrequency = (keyNumber: number): number => {
        const refFrequency = 440.0; // Frequency of A4
        return refFrequency * Math.pow(2, (keyNumber - 49) / 12);
    };


    calcFreq(y: number): number {
        try {
            let yN = this.normalize(
                y,
                this.notes.scale.length - 1,
                0,
                this.height,
                0
            );
            let yN_1 = yN - this.chordInterval;
            if (yN_1 < 0) {
                yN_1 = this.notes.scale.length - 1 + yN_1;
            }

            if (yN > this.notes.scale.length - 1 || yN < 0) {
                console.error("yN out of range", yN);
                yN = 0;
            }

            return this.notes.scale[yN].freq;
        }
        catch (err) {
            console.log(`Could not get frequency for key ${y}`)
            return 0;
        }
    }

    // Function to play a sequence of chords smoothly
    playChords = (chords: any[], duration: number) => {
        if (this.audioContext == null) return;
        let startTime = this.audioContext.currentTime; // Start in a little bit
        chords.forEach((note: number) => {
            const frequency = this.calculateFrequency(note);
            this.playFrequency(frequency, startTime, duration);
        })
    }

    play = (track: any[], duration: number) => {
        this.playChords(track, duration);
    }

    abstract playFrequency(frequency: number, startTime: number, duration: number): void;
}
