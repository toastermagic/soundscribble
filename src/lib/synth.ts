import { Instrument } from "./instrument";

export default class Synth extends Instrument {
    // Function to play a frequency for a given duration at a specific time
    playFrequency = (frequency: number, startTime: number, duration: number) => {
        if (this.audioContext == null) return;
        const oscillator = this.audioContext.createOscillator();
        const gainNode = this.audioContext.createGain();


        oscillator.type = "triangle";
        oscillator.frequency.setValueAtTime(frequency, startTime);

        // ADSR Envelope using GainNode
        const attack = 0.1;
        const decay = 0.3;
        const sustain = 0.5;
        const release = 0.2;

        gainNode.gain.setValueAtTime(0, startTime);
        gainNode.gain.linearRampToValueAtTime(1, startTime + attack);
        gainNode.gain.exponentialRampToValueAtTime(sustain, startTime + attack + decay);
        gainNode.gain.exponentialRampToValueAtTime(0.01, startTime + duration);

        oscillator.connect(gainNode);
        gainNode.connect(this.audioContext.destination);
        // gainNode.gain.setValueAtTime(1, startTime)
        // gainNode.gain.setValueAtTime(1, startTime + (duration * 0.9))
        // gainNode.gain.exponentialRampToValueAtTime(0.01, startTime + duration);

        oscillator.start(startTime);
        oscillator.stop(startTime + duration + release);
    };
}
