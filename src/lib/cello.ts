import { Instrument } from "./instrument";

export default class Cello extends Instrument {
    // Function to play a frequency for a given duration at a specific time
    playFrequency = (frequency: number, startTime: number, duration: number) => {
        if (this.audioContext == null) return;
        const fundamental = this.audioContext.createOscillator();
        const overtone = this.audioContext.createOscillator();
        const gainNode = this.audioContext.createGain();

        // Fundamental frequency using a sine wave
        fundamental.type = 'sine';
        fundamental.frequency.setValueAtTime(frequency, startTime);

        // Overtone to add richness (first overtone is double the fundamental frequency)
        overtone.type = 'sine';
        overtone.frequency.setValueAtTime(frequency * 2, startTime);

        // ADSR envelope using GainNode
        const attack = 0.4;  // slow attack
        const decay = 0.2;   // mild decay
        const sustain = 0.8; // high sustain
        const release = 0.5; // slow release

        gainNode.gain.setValueAtTime(0, startTime);
        gainNode.gain.linearRampToValueAtTime(1, startTime + attack);
        gainNode.gain.exponentialRampToValueAtTime(sustain, startTime + attack + decay);
        gainNode.gain.setValueAtTime(sustain, startTime + duration);
        gainNode.gain.exponentialRampToValueAtTime(0.01, startTime + duration + release);

        // Connect and play
        fundamental.connect(gainNode);
        overtone.connect(gainNode);
        gainNode.connect(this.audioContext.destination);
        fundamental.start(startTime);
        fundamental.stop(startTime + duration + release);
        overtone.start(startTime);
        overtone.stop(startTime + duration + release);
    };
}
